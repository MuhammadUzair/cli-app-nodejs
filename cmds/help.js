const menus = {
  main: `
      outside [command] <options>
  
      today .............. show user for today
      forecast ........... show 10-day user forecast
      version ............ show package version
      help ............... show help menu for a command`,

  today: `
      outside today <options>
  
      --user, -l ..... the user to use`,

  forecast: `
      outside forecast <options>
  
      --user, -l ..... the user to use`
};

module.exports = args => {
  const subCmd = args._[0] === 'help' ? args._[1] : args._[0];

  console.log(menus[subCmd] || menus.main);
};
