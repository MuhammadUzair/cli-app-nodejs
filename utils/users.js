const axios = require('axios');

module.exports = async user => {
  const results = await axios({
    method: 'get',
    url: 'https://jsonplaceholder.typicode.com/todos/1'
    // params: {
    //   format: 'json',
    //   q: user
    // }
  });

  return results.data;
};
