const ora = require('ora');
const getuser = require('../utils/users');

module.exports = async args => {
  const spinner = ora().start();
  console.log('today arags call  ', args);

  try {
    const user = args.user || args.l;
    const userAPI = await getuser(user);

    spinner.stop();

    console.log(`user res ${JSON.stringify(userAPI)}:`);
  } catch (err) {
    spinner.stop();

    console.error(err);
  }
};
